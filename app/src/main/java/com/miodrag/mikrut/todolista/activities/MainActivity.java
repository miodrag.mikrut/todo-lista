package com.miodrag.mikrut.todolista.activities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.adapter.RVadapterGroupLista;
import com.miodrag.mikrut.todolista.db.DatabaseHelper;
import com.miodrag.mikrut.todolista.db.model.Grupa;
import com.miodrag.mikrut.todolista.dialog.AboutDialog;
import com.miodrag.mikrut.todolista.tools.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RVadapterGroupLista.OnRVItemClick {

    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    List<String> drawerItems;
    ListView drawerList;
    private RecyclerView recyclerView;
    private DatabaseHelper databaseHelper;
    private RVadapterGroupLista rVadapterGroupLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("TODO Lista");
        fillData();
        setupToolbar();
        setupDrawer();
        setupRV();
//        add();
    }


    //Pocetak setup-a za toolbar
    // action bar prikazuje opcije iz meni.xml
    //uneti u action main.xml AppBarLayout i onda Toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create:
                Intent intent = new Intent(this,AddGrupa.class);
                startActivity(intent);
                break;
            case R.id.action_settings:
                Intent intent1 = new Intent(this,SettingsActivity.class);
                startActivity(intent1);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    //setuje toolbar
    private void setupToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_hamburger_white);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }
    //kraj setup-a za toolbar

    //popunjava listu za drawer
    private void fillData(){
        drawerItems = new ArrayList<>();
        drawerItems.add("Group list");
        drawerItems.add("Settings");
        drawerItems.add("About the app");
    }

    //podesavanje Drawera
    private void setupDrawer(){
        drawerList = findViewById(R.id.left_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, drawerItems));
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title = "Unknown";
                switch (i){
                    case 0:
                        title = "TODO Lista";

                        break;
                    case 1:
                        Intent settings = new Intent(MainActivity.this,SettingsActivity.class);
                        startActivity(settings);
                        title = "Settings";
                        setTitle(title);
                        break;
                    case 2:
                        AboutDialog dialog = new AboutDialog(MainActivity.this);
                        dialog.show();
                        title = "O aplikaciji";
                        break;
                    default:
                        break;
                }
                //drawerList.setItemChecked(i, true);
                setTitle(title);
                drawerLayout.closeDrawer(drawerList);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawerLayout,                   /* DrawerLayout object */
                toolbar,                        /* nav drawer image to replace 'Up' caret */
                R.string.app_name,           /* "open drawer" description for accessibility */
                R.string.app_name           /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }
        };
    }

    //dodaje u bazu radi testa
    private void add(){
        List<String> listaGrupa = new ArrayList<>();
        Grupa grupa = new Grupa("John");
        listaGrupa.add(grupa.getNaziv());
        listaGrupa.add(grupa.getNaziv());

        try {
            getDatabaseHelper().getmGrupaDao().create(grupa);

            //refresh list
            rVadapterGroupLista.setNewData(getDatabaseHelper().getmGrupaDao().queryForAll());
            Toast.makeText(this, "Podaci upisani u bazu ", Toast.LENGTH_SHORT).show();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onRVItemclick(Grupa grupa) {
        Toast.makeText(this, "Kliknuto na RV listu na "+ grupa.getNaziv(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,DetailGroup.class);
        intent.putExtra("grupa_id",grupa.getId());
        startActivity(intent);
    }

    //stavljanj RV adatera
    private void setupRV(){
        recyclerView = findViewById(R.id.rvLista);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        try {
            rVadapterGroupLista = new RVadapterGroupLista(getDatabaseHelper().getmGrupaDao().queryForAll(), this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        recyclerView.setAdapter(rVadapterGroupLista);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

}
