package com.miodrag.mikrut.todolista.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.db.DatabaseHelper;
import com.miodrag.mikrut.todolista.db.model.Grupa;
import com.miodrag.mikrut.todolista.db.model.Zadatak;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddGrupa extends AppCompatActivity {

    private EditText et_naziv;
    private EditText et_opis;
    private EditText et_datum;

    private TextView et_zadatak_title;
    private Button btn_add_actor;
    private Zadatak zadatak;
    private Grupa grupa;

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_grupa);

        et_naziv = findViewById(R.id.et_naziv_add_group);
        et_opis = findViewById(R.id.opis_add_grupa);
//        et_datum = findViewById(R.id.et_datum_add_group);
//        et_zadatak_title = findViewById(R.id.ev_zadatak_title_add_group);

        //dodavanje editovanog glumca uz proveru polja na button click
        btn_add_actor = findViewById(R.id.btn_add_grupa);
        btn_add_actor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validacija unosa svih polja sem za film i rating
                if(validateInput(et_naziv) && validateInput(et_opis)) {
                    add();
                    Toast.makeText(AddGrupa.this, ""+datum(), Toast.LENGTH_SHORT).show();
                    if(proveraPrefsPodesavanjaZaToast()) {
                        Toast.makeText(AddGrupa.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
                    }
                    if(proveraPrefsPodesavanjaZaNotifikacije()) {
                        Toast.makeText(AddGrupa.this, "Notifikacija da je upisano u bazi", Toast.LENGTH_SHORT).show();
                    }

                    Intent intent = new Intent(AddGrupa.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

    }
    //upisuje sve nove podatke u glumca
    private void add(){
        grupa = new Grupa();
        grupa.setNaziv(et_naziv.getText().toString().trim());
        grupa.setDatum(datum());

        try {
            getDatabaseHelper().getmGrupaDao().create(grupa);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //provera prefs settinga za checkbox notifikacije
    private boolean proveraPrefsPodesavanjaZaNotifikacije(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBox = prefs.getBoolean("notifications", true);
        return checkBox;
    }
    //provera prefs settingsa za checkbox toast
    private boolean proveraPrefsPodesavanjaZaToast(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBox = prefs.getBoolean("toast", true);
        return checkBox;
    }

    //vraca trenutni datum u string formatu
    private String datum() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
        Date currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
        return dateFormat.format(currentTime);
    }

    //validacija unosa za filma
    private boolean validateInput(EditText editText) {
        String titleInput = editText.getText().toString().trim();

        if (titleInput.isEmpty()) {
            editText.setError("Field can't be empty");
            return false;
        }else {
            editText.setError(null);
            return true;
        }
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MainActivity.class));
    }
}
