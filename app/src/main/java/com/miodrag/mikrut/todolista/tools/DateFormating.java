package com.miodrag.mikrut.todolista.tools;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormating {

    public static boolean dateValidatinion(String dateOfTask, String dateToCompare) {
        // Get Current Date Time
        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String getCurrentDateTime = sdf.format(c.getTime());


        Log.d("getCurrentDateTime", getCurrentDateTime);

        if (dateToCompare.compareTo(dateOfTask) < 0) {
            return true;
        } else {
            Log.d("Return", "getMyTime older than dateToCompare ");
            return false;
        }
    }

    public static String addToDate(int timeToAdd){
        // create a calendar
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, timeToAdd);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        return sdf.format(cal.getTime());
    }

    public static String currentDateToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
        Date currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
        return dateFormat.format(currentTime);
    }
}
