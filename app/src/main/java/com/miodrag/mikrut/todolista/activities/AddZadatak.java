package com.miodrag.mikrut.todolista.activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.db.DatabaseHelper;
import com.miodrag.mikrut.todolista.db.model.Grupa;
import com.miodrag.mikrut.todolista.db.model.Zadatak;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddZadatak extends AppCompatActivity {

    private EditText title;
    private EditText description;
    private RadioGroup radioGroup;
    private RadioButton priorityRadioButton;
    private EditText datum_zavrsvanja;
    private Button btn_add;
    private int grupa_id;
    private Grupa grupa = null;

    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_zadatak);

        title = findViewById(R.id.et_title_add_zadatak);
        description = findViewById(R.id.et_description_add_zadatak);
        radioGroup = findViewById(R.id.radio_group);
        priorityRadioButton = findViewById(R.id.radio_green);
        datum_zavrsvanja = findViewById(R.id.add_zadatak_datum_zavrsavanja);

        btn_add = findViewById(R.id.btn_add_add_task);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateZadatak(title) && validateZadatak(description)) {
                    checkRadioButton();
                    addZadatakToTheList();
                    finish();
                }
            }
        });
    }

    //take grupa_id i na osnovu toga ubacuje se film za tog glumca.
    //posle ubacivanja vraca se na DetailActivity
    private void addZadatakToTheList() {
        grupa_id =  getIntent().getIntExtra("grupa_id", 1);
        Toast.makeText(this, "Grupa id: "+grupa_id, Toast.LENGTH_SHORT).show();
        try {
            grupa = getDatabaseHelper().getmGrupaDao().queryForId(grupa_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Zadatak zadatak = new Zadatak();
        zadatak.setNaziv(title.getText().toString());
        zadatak.setOpis(description.getText().toString());
        zadatak.setVreme_kreiranja(datum());
        zadatak.setVreme_zavrsetka(datum_zavrsvanja.getText().toString());
        zadatak.setGrupa(grupa);
        zadatak.setPrioritet(priorityRadioButton.getText().toString());
        zadatak.setStatus("aktivan");

        try {
            getDatabaseHelper().getmZadatakDao().createIfNotExists(zadatak);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, DetailGroup.class);
        intent.putExtra("grupa_id", grupa_id);
        startActivity(intent);

    }

    //validacija unosa za filma
    private boolean validateZadatak(EditText editText) {
        String titleInput = editText.getText().toString().trim();

        if (titleInput.isEmpty()) {
            editText.setError("Field can't be empty");
            return false;
        }else {
            editText.setError(null);
            return true;
        }
    }

    //vraca trenutni datum u string formatu
    private String datum() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); //u ovom foramtu ce se ispisivati datum
        Date currentTime = Calendar.getInstance().getTime(); //vraca tremutno vreme za upis u bazu
        return dateFormat.format(currentTime);
    }

    //provera radio buttona
    private void checkRadioButton(){
        int radioId = radioGroup.getCheckedRadioButtonId();
        priorityRadioButton = findViewById(radioId);
    }

    //Metoda koja komunicira sa bazom podataka. Kopirati svugde gde se koristi baza
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        //nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MainActivity.class));
    }
}
