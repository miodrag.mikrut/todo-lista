package com.miodrag.mikrut.todolista.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class Tools {

    //provera prefs settinga za checkbox notifikacije
    public static boolean proveraPrefsPodesavanjaZaNotifikacije(Context context,String odabir){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean checkBox = prefs.getBoolean(odabir, true);
        return checkBox;
    }

    public static String proveraPrefsPodesavanjaZaFilter(Context context){
        String selektovano = null;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean checkBox = prefs.getBoolean("filter_checkbox", true);
        if(checkBox){
            selektovano = prefs.getString("pref_filter_list","");
            Log.d("REZ", "proveraPrefsPodesavanjaZaFilter: " + selektovano);
//          if(selektovano.equals("10")){}
//          if(selektovano.equals("60")){}
//          if(selektovano.equals("1440")){}
//          if(selektovano.equals("10080")){}
        }

        return selektovano;
    }


}
