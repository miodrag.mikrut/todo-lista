package com.miodrag.mikrut.todolista.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.db.model.Grupa;

import java.util.List;

public class RVadapterGroupLista extends RecyclerView.Adapter<RVadapterGroupLista.MyViewHolder> {

    private List<Grupa> listaGrupa;

    public OnRVItemClick listenerListaGrupa;

    public interface OnRVItemClick{
        void onRVItemclick(Grupa grupa);
    }

    public RVadapterGroupLista(List<Grupa> listaGrupa, OnRVItemClick listenerListaGrupa) {
        this.listaGrupa = listaGrupa;
        this.listenerListaGrupa = listenerListaGrupa;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNaziv;
        TextView datum;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvNaziv = itemView.findViewById(R.id.tv_recycler_naziv_zadatak);
            datum = itemView.findViewById(R.id.tv_recycler_datum_zadatak);
        }

        public void bind(final Grupa grupa, final OnRVItemClick listener) {
            tvNaziv.setText(grupa.getNaziv());
            datum.setText(grupa.getDatum() + "");

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(grupa);
                }
            });
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_single_item,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(listaGrupa.get(i), listenerListaGrupa);
    }

    @Override
    public int getItemCount() {
        return listaGrupa.size();
    }

    //dodavanje i refresh rv liste
    public void setNewData(List<Grupa> listaGrupa){
        this.listaGrupa.clear();
        this.listaGrupa.addAll(listaGrupa);
        notifyDataSetChanged();
    }

}
