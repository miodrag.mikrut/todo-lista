package com.miodrag.mikrut.todolista.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.List;

@DatabaseTable(tableName = "grupe")
public class Grupa {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "naziv")
    private String naziv;
    @DatabaseField(columnName = "datum")
    private String datum;

//    ne znam za cega sluzi ovo
//    @ForeignCollectionField
//    private List<String> oznake;

    @ForeignCollectionField(foreignFieldName = "grupa", eager = true)
    private ForeignCollection<Zadatak> zadaci;

    public Grupa() {
    }

    public Grupa(String naziv) {
        this.naziv = naziv;
//        this.datum = datum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    @Override
    public String toString() {
        return naziv;
    }
}
