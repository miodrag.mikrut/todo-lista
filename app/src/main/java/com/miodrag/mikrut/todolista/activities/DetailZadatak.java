package com.miodrag.mikrut.todolista.activities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.adapter.RVadapterGroupLista;
import com.miodrag.mikrut.todolista.db.DatabaseHelper;
import com.miodrag.mikrut.todolista.db.model.Grupa;
import com.miodrag.mikrut.todolista.db.model.Zadatak;
import com.miodrag.mikrut.todolista.dialog.AboutDialog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailZadatak extends AppCompatActivity {

    private DatabaseHelper databaseHelper;

    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    List<String> drawerItems;
    ListView drawerList;
    private RecyclerView recyclerView;
    private RVadapterGroupLista rVadapterGroupLista;
    private Zadatak zadatak;
    TextView tv_naziv;
    TextView tv_opis;
    TextView tv_kreiran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_zadatak);

        setTitle("TODO Lista");
        setupToolbar();
        getAndSetData();
    }

    //na osnovu intenta vadi glumac_id i povraci iz baze podatke za tog glumca i popunjava polja
    private void getAndSetData(){
        int grupa_id = getIntent().getIntExtra("objekat_id",1);
        try {
            zadatak = getDatabaseHelper().getmZadatakDao().queryForId(grupa_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tv_naziv = findViewById(R.id.detail_zadatak_tv_naziv);
        tv_opis = findViewById(R.id.detail_zadatak_tv_opis);
        tv_kreiran = findViewById(R.id.detail_zadatak_tv_vreme_kreiranja);

        tv_naziv.setText(zadatak.getNaziv());
        tv_kreiran.setText(zadatak.getVreme_kreiranja());


    }

    //Pocetak setup-a za toolbar
    // action bar prikazuje opcije iz meni.xml
    //uneti u action main.xml AppBarLayout i onda Toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail_zadatak, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this,DetailGroup.class);
                startActivity(intent);
                break;

            case R.id.action_close:
                close();
                Toast.makeText(this, "status " + zadatak.getStatus(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_delete:
                delete();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    //setuje toolbar
    private void setupToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }
    //kraj setup-a za toolbar




//zatvara zadatak, setuje status na neaktivan
    private void close(){
        zadatak.setStatus("Neaktivan");
        try {
            getDatabaseHelper().getmZadatakDao().update(zadatak);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Intent intent1 = new Intent(this,DetailGroup.class);
        startActivity(intent1);
    }

    private void delete(){
        try {
            getDatabaseHelper().getmZadatakDao().delete(zadatak);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Intent intent2 = new Intent(this,DetailGroup.class);
        startActivity(intent2);
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
