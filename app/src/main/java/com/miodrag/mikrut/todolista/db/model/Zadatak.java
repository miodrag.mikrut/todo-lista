package com.miodrag.mikrut.todolista.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "zadaci")
public class Zadatak {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "naziv")
    private String naziv;
    @DatabaseField(columnName = "opis")
    private String opis;
    @DatabaseField(columnName = "prioritet")
    private String prioritet;
    @DatabaseField(columnName = "vreme_kreiranja")
    private String vreme_kreiranja;
    @DatabaseField(columnName = "vreme_zavrsetka")
    private String vreme_zavrsetka;
    @DatabaseField(columnName = "status")
    private String status;
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private Grupa grupa;

    public Zadatak(String naziv) {
        this.naziv = naziv;
    }

    public Zadatak() {


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getPrioritet() {
        return prioritet;
    }

    public void setPrioritet(String prioritet) {
        this.prioritet = prioritet;
    }

    public String getVreme_kreiranja() {
        return vreme_kreiranja;
    }

    public void setVreme_kreiranja(String vreme_kreiranja) {
        this.vreme_kreiranja = vreme_kreiranja;
    }

    public String getVreme_zavrsetka() {
        return vreme_zavrsetka;
    }

    public void setVreme_zavrsetka(String vreme_zavrsetka) {
        this.vreme_zavrsetka = vreme_zavrsetka;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Grupa getGrupa() {
        return grupa;
    }

    public void setGrupa(Grupa grupa) {
        this.grupa = grupa;
    }

    @Override
    public String toString() {
        return naziv;
    }
}
