package com.miodrag.mikrut.todolista.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.db.model.Zadatak;

import java.util.List;

public class RVadapterZadatakLista extends RecyclerView.Adapter<RVadapterZadatakLista.MyViewHolder> {

    private List<Zadatak> listaZadataka;

    public OnRVItemClick listenerListaZadatak;

    public interface OnRVItemClick{
        void onRVItemclick(Zadatak zadatak);
    }

    public RVadapterZadatakLista(List<Zadatak> listaZadataka, OnRVItemClick listenerListaZadatak) {
        this.listaZadataka = listaZadataka;
        this.listenerListaZadatak = listenerListaZadatak;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvFirstName;
        TextView tvLastName;
        ImageView imageView;
        TextView datum;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
//            imageView = itemView.findViewById(R.id.iv_single_item);
            tvFirstName = itemView.findViewById(R.id.tv_recycler_naziv_zadatak);
            tvLastName = itemView.findViewById(R.id.tv_recycler_id_zadatak);
            datum = itemView.findViewById(R.id.tv_recycler_datum_zadatak);
        }

        public void bind(final Zadatak zadatak, final OnRVItemClick listener) {
            tvFirstName.setText(zadatak.getNaziv());
            tvLastName.setText(zadatak.getId() + ". ");
            datum.setText(zadatak.getVreme_kreiranja() + "");

            if(zadatak.getStatus() != null) {
                if (!zadatak.getStatus().equals("aktivan")) {
                    itemView.setBackgroundResource(R.color.not_active);

                } else if (zadatak.getPrioritet().equals("high")) {
                    itemView.setBackgroundResource(R.color.high);
                } else if (zadatak.getPrioritet().equals("low")) {
                    itemView.setBackgroundResource(R.color.low);
                } else {
                    itemView.setBackgroundResource(R.color.normal);
                }
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(zadatak);
                }
            });

            //proverava se datum i na osnovu toga se boji pozadina
//            if (!DateFormating.dateValidatinion(grupa.getDatum(), DateFormating.currentDateToString())){
//                itemView.setBackgroundResource(R.color.colorPrimary);
//            }{
//                itemView.setBackgroundResource(R.color.colorAccent);
//            }
            //kraj provere

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_single_item_zadatak,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(listaZadataka.get(i), listenerListaZadatak);
    }

    @Override
    public int getItemCount() {
        return listaZadataka.size();
    }

    //dodavanje i refresh rv liste
    public void setNewData(List<Zadatak> listaGrupa){
        this.listaZadataka.clear();
        this.listaZadataka.addAll(listaGrupa);
        notifyDataSetChanged();
    }

}
