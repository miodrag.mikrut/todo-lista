package com.miodrag.mikrut.todolista.activities;


import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.todolista.R;
import com.miodrag.mikrut.todolista.adapter.RVadapterZadatakLista;
import com.miodrag.mikrut.todolista.db.DatabaseHelper;
import com.miodrag.mikrut.todolista.db.model.Grupa;
import com.miodrag.mikrut.todolista.db.model.Zadatak;
import com.miodrag.mikrut.todolista.tools.DateFormating;
import com.miodrag.mikrut.todolista.tools.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailGroup extends AppCompatActivity implements RVadapterZadatakLista.OnRVItemClick{

    public static final String TAG = "REZ";

    private DatabaseHelper databaseHelper;
    private Toolbar toolbar;
    private TextView tv_first_name;
    private TextView tv_last_name;
    private TextView tv_rodjen;
    private ImageButton imageButton;
    private Grupa grupa;
    private Button btn_add_zadatak;
    private Button btn_edit;
    private List<Zadatak> lista_zadataka;
    private List<Zadatak> lista_zadataka_filtrirana;
    private RecyclerView recyclerView;
    private RVadapterZadatakLista rVadapterZadatakLista;

    List<Zadatak> zadaci;


    public static final int notificationID = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_group);

        setupToolbar();
        getAndSetData(getIntent());
        setupRV();

        Button btn_delete = findViewById(R.id.btn_delete_actor);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteGroup();
            }
        });
        btn_add_zadatak = findViewById(R.id.btn_add_zadatak);
        btn_add_zadatak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addZadatak();
            }
        });

        Button btn_edit = findViewById(R.id.btn_edit_actor);
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editGroup();
            }
        });
    }


    //Pocetak setup-a za toolbar
    // action bar prikazuje opcije iz meni.xml
    //uneti u action main.xml AppBarLayout i onda Toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.action_create:
                addZadatak();

                Toast.makeText(this, "Add new zadatak", Toast.LENGTH_SHORT).show();

                if(proveraPrefsPodesavanjaZaToast()){
                    Toast.makeText(this, "Add new movie to the actor", Toast.LENGTH_SHORT).show();
                }
                if(proveraPrefsPodesavanjaZaNotifikacije()){
                    setupNotification(grupa, " add movie to the actor "+ grupa);
                }
                break;
            case R.id.action_edit:
                Toast.makeText(this, "Edit desired fields and click EDIT button", Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "Action update executed.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_delete:
                deleteGroup();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //setuje toolbar
    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }
    //kraj setup-a za toolbar

    private void editGroup(){
//        Intent intent = new Intent(DetailGroup.this, EditActor.class);
//        intent.putExtra("grupa_id", group.getId());
//        startActivity(intent);
    }
    //brise glumca i sve filmove koji su vezani za id tog glumca
    private void deleteGroup() {
        AlertDialog dialogDelete = new AlertDialog.Builder(this)
                .setTitle("Delete group")
                .setMessage("Are you sure you want to permanently delete the group?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            List<Zadatak> movies = getDatabaseHelper().getmZadatakDao().queryForEq("grupa_id", grupa.getId());

                            getDatabaseHelper().getmGrupaDao().delete(grupa);
                            for(Zadatak movie:movies) {
                                getDatabaseHelper().getmZadatakDao().delete(movie);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if(proveraPrefsPodesavanjaZaToast()) {
                            Toast.makeText(DetailGroup.this, grupa + " deleted.", Toast.LENGTH_SHORT).show();
                        }
                        if(proveraPrefsPodesavanjaZaNotifikacije()){
                            setupNotification(grupa, " deleted from the list");
                            Toast.makeText(DetailGroup.this, "Notify - " + grupa + " deleted.", Toast.LENGTH_SHORT).show();
                        }
                        Intent intent = new Intent(DetailGroup.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel",null)
                .show();

    }

    private void addZadatak(){
        int grupa_id = grupa.getId();
        rVadapterZadatakLista.notifyDataSetChanged();
        Intent intent = new Intent(this, AddZadatak.class);
        intent.putExtra("grupa_id",grupa_id);
        startActivity(intent);
    }

    //na osnovu intenta vadi glumac_id i povraci iz baze podatke za tog glumca i popunjava polja
    private void getAndSetData(Intent intent){
        int grupa_id = intent.getIntExtra("grupa_id",1);
        zadaci = new ArrayList<>();
        try {
            grupa = getDatabaseHelper().getmGrupaDao().queryForId(grupa_id);
            zadaci = getDatabaseHelper().getmZadatakDao().queryForEq("grupa_id",grupa_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tv_first_name = findViewById(R.id.tv_naziv_detail_group);
        tv_last_name = findViewById(R.id.tv_opis_detail_group);
        tv_rodjen = findViewById(R.id.tv_kreiran_detail_group);

        tv_first_name.setText(grupa.getNaziv());
        tv_rodjen.setText(grupa.getDatum());


    }

    //provera prefs settinga za checkbox notifikacije
    private boolean proveraPrefsPodesavanjaZaNotifikacije(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBox = prefs.getBoolean("notifications", true);
        return checkBox;
    }
    //provera prefs settingsa za checkbox toast
    private boolean proveraPrefsPodesavanjaZaToast(){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBox = prefs.getBoolean("toast", true);
        return checkBox;
    }

    private void setupNotification(Grupa glumac, String text){
        String textForTheNotification = text;
        Notification builder = new Notification.Builder(this)
                .setContentTitle("Notification")
                .setContentText(glumac.toString() + textForTheNotification)
                .setSmallIcon(R.drawable.ic_delete_group)
                .build();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

// notificationID allows you to update the notification later on.
        mNotificationManager.notify(notificationID, builder);

    }


    //stavljanj RV adatera
    private void setupRV(){
        recyclerView = findViewById(R.id.rvLista_zadatak);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        lista_zadataka = new ArrayList<>();
        try {
            lista_zadataka = getDatabaseHelper().getmZadatakDao().queryForEq("grupa_id",grupa.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        rVadapterZadatakLista = new RVadapterZadatakLista(lista_zadataka, this);
        recyclerView.setAdapter(rVadapterZadatakLista);

    }

    @Override
    public void onRVItemclick(Zadatak zadatak) {
        Intent intent = new Intent(this,DetailZadatak.class);
        intent.putExtra("objekat_id",zadatak.getId());
        startActivity(intent);
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MainActivity.class));
    }


}
